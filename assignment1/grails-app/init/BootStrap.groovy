import com.testapp.Role
import com.testapp.User
import com.testapp.UserRole

class BootStrap {

//    def init = { servletContext ->
//    }
    def destroy = {
    }
    def init = {

        def adminRole = new Role(authority: 'ROLE_ADMIN').save()
        def userRole = new Role(authority: 'ROLE_USER').save()

        def admin = new User(username: 'admin', password: 'admin').save()

        UserRole.create admin, adminRole

        UserRole.withSession {
            it.flush()
            it.clear()
        }

        assert User.count() == 1
        assert Role.count() == 2
        assert UserRole.count() == 1
    }
}
