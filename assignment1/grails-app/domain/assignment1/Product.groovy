package assignment1

class Product {
    String productName
    String productDescription
    String imageSmall
    String imageLarge
    Date dateAdded
    Double price
    Boolean productStatus

    static constraints = {
        imageSmall nullable: true
        imageLarge nullable: true
    }
}
