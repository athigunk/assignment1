<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Gun's</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
    <sec:ifLoggedIn>
        <content tag="nav">
            <li><a href="${createLink(uri:'/logout')}">Logout</a></li>
        </content>
    </sec:ifLoggedIn>
    <sec:ifNotLoggedIn>
        <content tag="nav">
            <li><a href="${createLink(uri:'/login')}">Login</a></li>
        </content>
    </sec:ifNotLoggedIn>

    <div class="svg" role="presentation">
        <div class="grails-logo-container">
            <asset:image src="grails-cupsonly-logo-white.svg" class="grails-logo"/>
        </div>
    </div>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>Welcome Admin!!</h1>

            <div id="controllers" role="navigation">
                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4">
                    <a type="button" class="btn btn-success" style="text-decoration: none;" role="button" href="${createLink(uri: '/product')}">Product List</a>
                </div>
                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4">
                    <a type="button" class="btn btn-info" style="text-decoration: none;" role="button" href="${createLink(uri: '/user')}">User Config</a>
                </div>
                <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4">
                    <a type="button" class="btn btn-warning" style="text-decoration: none;" role="button" href="${createLink(uri: '/role')}">Role Config</a>
                </div>
                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                </div>
            </div>
        </section>
    </div>

</body>
</html>
