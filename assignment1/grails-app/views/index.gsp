<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Gun's</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
    <content tag="nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Application Status <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Environment: ${grails.util.Environment.current.name}</a></li>
                <li><a href="#">App profile: ${grailsApplication.config.grails?.profile}</a></li>
                <li><a href="#">App version:
                    <g:meta name="info.app.version"/></a>
                </li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Grails version:
                    <g:meta name="info.app.grailsVersion"/></a>
                </li>
                <li><a href="#">Groovy version: ${GroovySystem.getVersion()}</a></li>
                <li><a href="#">JVM version: ${System.getProperty('java.version')}</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Artefacts <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Controllers: ${grailsApplication.controllerClasses.size()}</a></li>
                <li><a href="#">Domains: ${grailsApplication.domainClasses.size()}</a></li>
                <li><a href="#">Services: ${grailsApplication.serviceClasses.size()}</a></li>
                <li><a href="#">Tag Libraries: ${grailsApplication.tagLibClasses.size()}</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Installed Plugins <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
                    <li><a href="#">${plugin.name} - ${plugin.version}</a></li>
                </g:each>
            </ul>
        </li>
        <sec:ifLoggedIn>
                <li><a href="${createLink(uri:'/logout')}">Logout</a></li>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
                <li><a href="${createLink(uri:'/login')}">Login</a></li>
        </sec:ifNotLoggedIn>
    </content>

    <div class="svg" role="presentation">
        <div class="grails-logo-container">
            <asset:image src="grails-cupsonly-logo-white.svg" class="grails-logo"/>
        </div>
    </div>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>Welcome to my first Grails application</h1>

            <div id="controllers" role="navigation">
                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6" style="margin-left:auto; margin-right:auto; align:  ;">
                    <a type="button" class="btn btn-success" style="text-decoration: none;" role="button" href="${createLink(uri: '/product')}">Product List</a>
                </div>
                <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                    <a type="button" class="btn btn-danger" style="text-decoration: none;" role="button" href="${createLink(uri: '/admin')}">Admin Page</a><i>&nbsp;&nbsp;(login required)</i>
                </div>
            </div>
        </section>
    </div>

</body>
</html>
