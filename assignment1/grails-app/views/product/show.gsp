<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Product Info</title>
    </head>
    <body>
        <sec:ifLoggedIn>
            <content tag="nav">
                <li><a href="${createLink(uri:'/logout')}">Logout</a></li>
            </content>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <content tag="nav">
                <li><a href="${createLink(uri:'/login')}">Login</a></li>
            </content>
        </sec:ifNotLoggedIn>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}">Home</a></li>
                <li><g:link class="list" action="index">Product List</g:link></li>
                <li><g:link class="create" action="create">Add Product</g:link></li>
            </ul>
        </div>
        <div id="show-product" class="content scaffold-show" role="main">
            <h1><b>${product.productName}</b></h1>
            <div class="row">
                <img style="width: 400px;" src="${resource(dir:'images', file:product.imageLarge)}" />
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                    Description:
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    ${product.productDescription}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                    Price:
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    ${product.price}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                    Date Added:
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    ${product.dateAdded}
                </div>
            </div>
            <sec:ifLoggedIn>
                <div class="row">
                    <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                        Status:
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <g:if test="${product.productStatus}">Published</g:if>
                        <g:else>Drafted</g:else>
                    </div>
                </div>
            </sec:ifLoggedIn>

            <g:form resource="${this.product}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.product}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
