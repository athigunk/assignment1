<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Products</title>
    </head>
    <body>
        <sec:ifLoggedIn>
            <content tag="nav">
                <li><a href="${createLink(uri:'/logout')}">Logout</a></li>
            </content>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <content tag="nav">
                <li><a href="${createLink(uri:'/login')}">Login</a></li>
            </content>
        </sec:ifNotLoggedIn>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}">Home</a></li>
                <li><g:link class="create" action="create">Add Product</g:link></li>
            </ul>
        </div>
        <div id="list-product" class="content scaffold-list" role="main">
            <h1>Available Product(s)</h1>
            <table>
                <g:if test="${loggedIn}">
                    <th class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></th><th class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Name</th><th class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Price</th><th class="col-xs-5 col-sm-5 col-md-5 col-lg-5">Status</th>
                </g:if>
                <g:else>
                    <th class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></th><th class="col-xs-1 col-sm-1 col-md-1 col-lg-1">Name</th><th class="col-xs-6 col-sm-6 col-md-6 col-lg-6">Price</th>
                </g:else>
                <g:each in="${products}" var="product" status="i">
                    <tr>
                        <td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a href="${createLink(uri: '/product/show/'+product.id)}"><img style="height: 100px;" src="${resource(dir:'images', file:product.imageSmall)}" /></td>
                        <td class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><a href="${createLink(uri: '/product/show/'+product.id)}"><b>${product.productName}</b></a></td>
                        <td class="col-xs-1 col-sm-1 col-md-1 col-lg-1">${product.price}</td>
                        <sec:ifLoggedIn>
                            <td class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                <g:if test="${product.productStatus}">Published</g:if>
                                <g:else>Drafted</g:else>
                            </td>
                        </sec:ifLoggedIn>
                    </tr>
                </g:each>
            </table>
        </div>
    </body>
</html>