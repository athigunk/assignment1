<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Edit</title>
    </head>
    <body>
        <sec:ifLoggedIn>
            <content tag="nav">
                <li><a href="${createLink(uri:'/logout')}">Logout</a></li>
            </content>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <content tag="nav">
                <li><a href="${createLink(uri:'/login')}">Login</a></li>
            </content>
        </sec:ifNotLoggedIn>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}">Home</a></li>
                <li><g:link class="list" action="index">Product List</g:link></li>
                <li><g:link class="create" action="create">Add Product</g:link></li>
            </ul>
        </div>
        <div id="edit-product" class="content scaffold-edit" role="main">
            <h1>Edit</h1>
            <g:hasErrors bean="${this.product}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.product}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form id="${product.id}" action="update" method="post" enctype="multipart/form-data">
                <fieldset class="form">
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                            Product Name:
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                            <g:textField name="productName" value="${product.productName}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                            Description:
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                            <g:textField name="productDescription" value="${product.productDescription}"/><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                            Price:
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                            <g:field type="number" step="0.25" name="price" value="${product.price}"/><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                            Date Added:
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                            <g:datePicker name="dateAdded" value="${product.dateAdded}"/><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2 col-sm-3 col-md-2 col-lg-2">
                            Status:
                        </div>
                        <div class="col-xs-3 col-sm-9 col-md-10 col-lg-10">
                            <g:checkBox name="productStatus" value="${product.productStatus}"/> Publish
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                            Image:
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                            <g:field type="file" name="fileLarge"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                            Small Image:
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                            <g:field type="file" name="fileSmall"/>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
