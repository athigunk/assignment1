package assignment1

import grails.plugin.springsecurity.annotation.Secured

class AdminController {

    @Secured('ROLE_ADMIN')
    def index() {
        render view: '/admin'
    }
}
