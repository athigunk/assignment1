package assignment1

import grails.plugin.springsecurity.annotation.Secured

class ProductController {

    def springSecurityService

    def index() {
        def loggedIn = springSecurityService.isLoggedIn()
        def products = Product.findAllWhere(productStatus: true)
        if (loggedIn) {
             products = Product.list()
        }
        render view:'index',model: [products:products, loggedIn:loggedIn]
    }

    def show(Product product) {
        def loggedIn = springSecurityService.isLoggedIn()
        if (loggedIn) {
            if(product) respond product
            else redirect url: '/product'
        } else {
            if(product) {
                if (product.productStatus) respond product
                else redirect url: '/product'
            }
            else redirect url: '/product'
        }
    }

    @Secured('IS_AUTHENTICATED_FULLY')
    def create() {
        respond new Product(params)
    }

    @Secured('IS_AUTHENTICATED_FULLY')
    def save(Product product) {
        if (product == null) {
            notFound()
            return
        }


        if (product.hasErrors()) {
            respond product.errors, view:'create'
            return
        }

        product.save flush:true

        def okContentTypes = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif']

        def f = request.getFile('fileSmall')
        if (okContentTypes.contains(f.getContentType())) {
            def webrootDir = servletContext.getRealPath("/") + "images/" //app directory
            product.imageSmall = product.id + "_small_" + f.filename
            File fileDest = new File(webrootDir + product.imageSmall)
            f.transferTo(fileDest)
        }
        else {
            product.imageSmall = "no_preview.png"
        }

        def f2 = request.getFile('fileLarge')
        if (okContentTypes.contains(f2.getContentType())) {
            def webrootDir2 = servletContext.getRealPath("/") + "images/" //app directory
            product.imageLarge = product.id + "_" + f2.filename
            File fileDest2 = new File(webrootDir2 + product.imageLarge)
            f2.transferTo(fileDest2)
        }
        else {
            product.imageLarge = "no_preview.png"
        }

        redirect product
    }

    @Secured('IS_AUTHENTICATED_FULLY')
    def edit(Product product) {
        respond product
    }

    @Secured('IS_AUTHENTICATED_FULLY')
    def update(Product product) {
        if (product == null) {
            notFound()
            return
        }

        if (product.hasErrors()) {
            respond product.errors, view:'edit'
            return
        }

        product.save flush:true

        def okContentTypes = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif']

        def f = request.getFile('fileSmall')
        if (okContentTypes.contains(f.getContentType())) {
            def webrootDir = servletContext.getRealPath("/") + "images/" //app directory
            product.imageSmall = product.id + "_small_" + f.filename
            File fileDest = new File(webrootDir + product.imageSmall)
            f.transferTo(fileDest)
        }
        else {
            //
        }

        def f2 = request.getFile('fileLarge')
        if (okContentTypes.contains(f2.getContentType())) {
            def webrootDir2 = servletContext.getRealPath("/") + "images/" //app directory
            product.imageLarge = product.id + "_" + f2.filename
            File fileDest2 = new File(webrootDir2 + product.imageLarge)
            f2.transferTo(fileDest2)
        }
        else {
            //
        }

        redirect product
    }

    @Secured('IS_AUTHENTICATED_FULLY')
    def delete(Product product) {

        if (product == null) {
            notFound()
            return
        }

        product.delete flush:true

        redirect action:"index", method:"GET"
    }

    protected void notFound() {
        redirect action: "index", method: "GET"
    }
}
