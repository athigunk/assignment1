<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title>Products</title>
    </head>
    <body>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}">Home</a></li>
                <li><g:link class="create" action="create">Add Product</g:link></li>
            </ul>
        </div>
        <div id="list-product" class="content scaffold-list" role="main">
            <h1>Available Product(s)</h1>
            <table>
                <th></th><th>Name</th><th>Price</th>
                <g:each in="${products}" var="product" status="i">
                    <tr>
                        <td><a href="${createLink(uri: '/product/show/'+product.id)}"><img style="height: 100px;" src="${resource(dir:'images', file:product.imageSmall)}" /></td>
                        <td><a href="${createLink(uri: '/product/show/'+product.id)}">${product.productName}</a></td>
                        <td>${product.price}</td>
                    </tr>
                </g:each>
            </table>
        </div>
    </body>
</html>